package com.sv.collectioneg;

import java.util.ArrayList;

public class ExampleArrayList {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(5);
		list.add(9);

		System.out.println(list); // 1, 5, 9

		System.out.println(list.size()); // 3

		System.out.println(list.isEmpty()); // false

		System.out.println(list.contains(5));

		System.out.println(list.indexOf(9));

	}

}
