package com.sv.arrayeg;

public class DynArrayInitialization {
	public static void main(String[] args) {
		int[] nums = { 10, 30, 50 };

		for (int i : nums) {
			System.out.println(i);
		}
		System.out.println();
		// using for loop

		for (int i = 0; i < nums.length; i++) {
			System.out.println(nums[i]);
		}
	}
}
