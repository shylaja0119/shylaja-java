package com.sv.arrayeg;

public class Employee {
	int empNo;
	String empName;
	double salary;

	Employee(int empNo, String empName, double salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + "]";
	}

	public static void main(String[] args) {
		Employee e1 = new Employee(1, "Alpha", 100);
		Employee e2 = new Employee(2, "Beta", 200);

		Employee[] employees = new Employee[3];
		employees[0] = e1;
		employees[1] = e2;

		for (Employee employee : employees) {
			System.out.println(employee);
		}

	}

}
