package com.sv.upcasting;

public class Fruit {
	String color;
	int noOfSeeds;

	Fruit(String color, int noOfSeeds) {
		super();
		this.color = color;
		this.noOfSeeds = noOfSeeds;
	}

	void clean() {
		System.out.println(this.getClass().getSimpleName() + "is being cleaned");
	}
}
