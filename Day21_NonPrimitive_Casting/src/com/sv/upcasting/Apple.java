package com.sv.upcasting;

public class Apple extends Fruit {

	Apple(String color, int noOfSeeds) {
		super(color, noOfSeeds);
	}

	void prepareShake() {
		System.out.println("preparing apple milkshake");
	}
}
