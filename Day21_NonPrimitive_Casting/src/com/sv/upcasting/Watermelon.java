package com.sv.upcasting;

public class Watermelon extends Fruit {

	Watermelon(String color, int noOfSeeds) {
		super(color, noOfSeeds);
	}

	void prepareJuice() {
		System.out.println("preparing watermelon juice");
	}

}
