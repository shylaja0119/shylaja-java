package com.sv.upcasting;

public class Shop {
	Fruit sell(int option) {
		if (option == 1) {
			Apple a = new Apple("Red", 2);
			return a;
		} else if (option == 2) {
			Watermelon w = new Watermelon("Green", 50);
			return w;
		} else {
			return null;
		}
	}

	public static void main(String[] args) {
		Shop s = new Shop();
		Fruit f = s.sell(1); // Fruit f = new Apple("red",2)
		if (f != null) {
			f.clean();
			Apple a = (Apple) f;
			a.prepareShake();
		} else {
			System.out.println("no such fruit option");
		}

		// instanceof - will check type of object
		Fruit f1 = s.sell(2); // Fruit f = new Apple("red",2) ; upcasting
		if (f1 != null) {
			f1.clean();
			if (f1 instanceof Apple) {
				Apple a = (Apple) f1;
				a.prepareShake();
			} else if (f1 instanceof Watermelon) {
				Watermelon w = (Watermelon) f1;
				w.prepareJuice();
			}
		} else {
			System.out.println("no such fruit option");
		}
	}

}
