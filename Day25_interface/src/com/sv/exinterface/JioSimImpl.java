package com.sv.exinterface;

public class JioSimImpl implements ISim {

	@Override
	public void call() {
		System.out.println("Jio Call");
	}

	@Override
	public void sms() {
		System.out.println("Jio sms");
	}

	@Override
	public void browse() {
		System.out.println("jio browse");
	}

	public void jioTV() {
		System.out.println("jio TV");
	}
}
