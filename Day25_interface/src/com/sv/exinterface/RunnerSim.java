package com.sv.exinterface;

public class RunnerSim {
	public static void main(String[] args) {
		ISim i = new BsnlSimImpl();
		i.browse();
		i.call();
		i.sms();

		if (i instanceof AirtelSimImpl) {
			AirtelSimImpl a = (AirtelSimImpl) i;
			a.airtelTV();
		} else if (i instanceof JioSimImpl) {
			JioSimImpl j = (JioSimImpl) i;
			j.jioTV();
		} else if (i instanceof BsnlSimImpl) {
			BsnlSimImpl b = (BsnlSimImpl) i;
			b.connectWithSupport();

		}

	}

}
