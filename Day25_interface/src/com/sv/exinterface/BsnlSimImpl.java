package com.sv.exinterface;

public class BsnlSimImpl implements ISim {

	@Override
	public void call() {
		System.out.println("BSNL call");
	}

	@Override
	public void sms() {
		System.out.println("BSNL sms");
	}

	@Override
	public void browse() {
		System.out.println("BSNL browse");
	}

	public void connectWithSupport() {
		System.out.println("Please connect with support");
	}
}
