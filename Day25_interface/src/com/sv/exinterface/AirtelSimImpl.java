package com.sv.exinterface;

public class AirtelSimImpl implements ISim {

	@Override
	public void call() {
		System.out.println("airtel call");
	}

	@Override
	public void sms() {
		System.out.println("airtel sms");
	}

	@Override
	public void browse() {
		System.out.println("airtel browse");
	}

	public void airtelTV() {
		System.out.println("airtel TV");
	}

}
