package com.sv.assoeg;

public class Name {
	String fn;
	String ln;

	Name(String fn, String ln) {
		super();
		this.fn = fn;
		this.ln = ln;
	}

	@Override
	public String toString() {
		return "Name [fn=" + fn + ", ln=" + ln + "]";
	}

}
