package com.sv.assoeg;

public class Student {
	int sNo;
	Name name;

	@Override
	public String toString() {
		return "Student [sNo=" + sNo + ", name=" + name + "]";
	}

	public static void main(String[] args) {
		Student s = new Student();
		Name n = new Name("Shyl", "venk");
		s.name = n;
		System.out.println(s);
		s.sNo = 100;
		s.name.fn = "Savitha";
		System.out.println(s);

	}

}
