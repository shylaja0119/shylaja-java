package com.sv.sampleconstructor;

public class Student {
	int rollNo;
	String name;
	int age;
	double avgMarks;

	// custom OR 3 param constructor
	// ALT + Shft +S +o
	public Student(int rollNo, String name, int age) {
		this.rollNo = rollNo;
		this.name = name;
		this.age = age;

	}

	// to print content of objects
	// Alt + shft + s + s
	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", name=" + name + ", age=" + age + ", avgMarks=" + avgMarks + "]";
	}

	public static void main(String[] args) {
		// use tab and ctrl+1 shortcuts to create new obj
		Student stu1 = new Student(101, "shyl", 20);
		// order of variables inside stu1 will depend on toSTring()
		System.out.println(stu1);
		stu1.avgMarks = 65;
		System.out.println(stu1);
	}

}
