package com.sv.sampleconstructor;

public class Mobile {
	String color;
	int storage;

	@Override
	public String toString() {
		return "Mobile [color=" + color + ", storage=" + storage + "]";
	}

	public static void main(String[] args) {
		Mobile m1 = new Mobile();
		Mobile m2 = new Mobile();
		System.out.println(m1);

		m2.storage = 128;
		System.out.println(m2);
	}

}
