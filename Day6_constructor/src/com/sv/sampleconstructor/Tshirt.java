package com.sv.sampleconstructor;

public class Tshirt {

	String color;
	int size;
	String brand;

	// constructor 1
	public Tshirt(String color) {
		super();
		this.color = color;
	}

	// constructor 2
	public Tshirt(String color, int size) {
		super();
		this.color = color;
		this.size = size;
	}

	// Constructor 3
	public Tshirt(String color, int size, String brand) {
		super();
		this.color = color;
		this.size = size;
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "Tshirt [color=" + color + ", size=" + size + ", brand=" + brand + "]";
	}

	public static void main(String[] args) {
		Tshirt T1 = new Tshirt("Red");
		System.out.println(T1);

		Tshirt T2 = new Tshirt("Grey", 38);
		System.out.println(T2);

		Tshirt T3 = new Tshirt("Sky Blue", 40, "Levis");
		System.out.println(T3);
	}

}
