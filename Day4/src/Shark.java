
class Shark {

	String name = "Shark";
	String color = "Green";
	int age = 5;
	boolean isBabyShark = true;

	void swim() {
		System.out.println("shark is swimming");
	}

	void sleep() {
		System.out.println("Shark is sleeping");
	}

	public static void main(String[] args) {
		Shark s1 = new Shark();
		Shark s2 = new Shark();
		Shark s3 = new Shark();
		Shark s4 = new Shark();
		Shark s5 = new Shark();

		s1.name = "Shark1";
		System.out.println(s1.name); // Shark1
		s2.name = "Shark2";
		System.out.println(s2.name); // Shark2

		s3.color = "Blue";
		System.out.println(s3.color); // Blue
		System.out.println(s2.color); // Green

		s4.age = 7;
		System.out.println(s4.age); // 7
		System.out.println(s3.age); // 5

		s5.isBabyShark = false;
		System.out.println(s4.isBabyShark); // true
		System.out.println(s5.isBabyShark); // false

		s1.swim();
		s2.sleep();
		s3.swim();
		s4.sleep();
		s5.sleep();
	}

}
