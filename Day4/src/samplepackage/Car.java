package samplepackage;

public class Car {
	
	String brand = "Mercedes";
	double price = 50000;
	int seatingCapacity = 4;
	String color = "Yellow";
	
	void start() {
		System.out.println("car started");
	}
	
	void stop() {
		System.out.println("car stopped");
	}
	
	public static void main(String[] args) {
		Car car1 = new Car();
		Car car2 = new Car();
		Car car3 = new Car();
		
		System.out.println(car1.color);
		System.out.println(car2.color);
		car3.color = "Red";
		System.out.println(car3.color);
		System.out.println(car1.price);
		car1.price = 75000;
		System.out.println(car1.price);
		
		car1.start();
		car3.start();
		car1.stop();
	}
	

}
