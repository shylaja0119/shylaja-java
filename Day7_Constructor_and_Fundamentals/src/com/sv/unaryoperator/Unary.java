package com.sv.unaryoperator;

public class Unary {

	public static void main(String[] args) {

		int x = 0;
		int y = ++x + x++ + ++x;
		int z = ++y + y++ + x++;
		int a = --y + --z;
		System.out.println(x); // 4
		System.out.println(y); // 6
		System.out.println(z); // 14
		System.out.println(a); // 20

	}

}
