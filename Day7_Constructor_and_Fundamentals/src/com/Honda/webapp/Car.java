package com.Honda.webapp;

public class Car {
	String brand = "Honda";
	String color;
	char variant;
	double mileage;
	
	//cons1
	public Car(String color, char variant, double mileage) {
		
		this.color = color;
		this.variant = variant;
		this.mileage = mileage;
	}
	//cons2
	public Car(String color) {
		
		this.color = color;
		this.variant = 'p';
		this.mileage = 5.5;
	}
	//cons3
	public Car() {
		this.color = "black";
		this.variant = 'd';
		this.mileage = 5;
	}
		
	@Override
	public String toString() {
		return "Car [brand=" + brand + ", color=" + color + ", variant=" + variant + ", mileage=" + mileage + "]";
	}
	public static void main(String[] args) {
		Car c1 = new Car();
		System.out.println(c1); //black , d, 5.0
		
		Car c2 = new Car("Maroon");
		System.out.println(c2); // maroon, p , 5.5
		
		Car c3 = new Car("Blue", 'd', 8.4);
		System.out.println(c3);// Blue , d, 8.4
	}
	

}
