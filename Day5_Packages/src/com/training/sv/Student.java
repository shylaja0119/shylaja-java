package com.training.sv;

public class Student {

	// instance variables
	String name; // null
	int age; // 0
	String CollegeName = "xyzIT";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Student s1 = new Student();
		Student s2 = new Student();
		// addr of obj
		System.out.println(s1);
		System.out.println(s1.name);// null
		s1.name = "asdf";
		System.out.println(s1.name);// asdf

		System.out.println(s1.age);// 0
		s2.age = 25;
		System.out.println(s2.age);// 25

	}

}
