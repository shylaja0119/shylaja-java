package com.training.sv.constructor;

public class Book {

	int bookId;
	String name;
	double price;
	String language;

	// Constructor - source > Generate cons using fields
	// Alt + shift + s + c
	Book(int bookId, String name, double price, String language) {

		this.bookId = bookId;
		this.name = name;
		this.price = price;
		this.language = language;

	}

	public static void main(String[] args) {

		Book b1 = new Book(123, "abc", 150.0, "Kannada");
		Book b2 = new Book(456, "pqr", 100.0, "Tamil");
		System.out.println(b1);// memory address of b1 obj/ var
		System.out.println(b1.language); // Kannada
		System.out.println(b2.price); // 100
		b1.price = 100;
		System.out.println(b1.price); // 100

	}

}
