package com.sv.objectarray;

public class Car {
	String brand = "Honda";
	double price = 1000.0;

	@Override
	public String toString() {
		return "Car [brand=" + brand + ", price=" + price + "]";
	}

}
