package com.sv.objectarray;

public class RunnerObjectArray {
	public static void main(String[] args) {
		Object[] objs = new Object[3];

		Employee e1 = new Employee();
		Student s1 = new Student();
		Car c1 = new Car();

		objs[0] = e1;
		objs[1] = s1;
		objs[2] = c1;

		for (Object ob : objs) {
			if (ob instanceof Employee) {
				Employee e2 = (Employee) ob;
				System.out.println(e2.empNo);
			} else if (ob instanceof Student) {
				Student s2 = (Student) ob;
				System.out.println(s2.name);
			} else if (ob instanceof Car) {
				Car c2 = (Car) ob;
				System.out.println(c2.brand);
			}
		}
	}

}
