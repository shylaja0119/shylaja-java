package com.sv.overridingtostring;

public class Employee {
	int empId;
	String name;
	double salary;

	Employee(int empId, String name, double salary) {
		super();
		this.empId = empId;
		this.name = name;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return name + " " + salary;
	}

	public static void main(String[] args) {
		Employee e1 = new Employee(25, "Alpha", 30000);
		System.out.println(e1);

		Employee e2 = new Employee(27, "Beta", 40000);
		System.out.println(e2);
	}

}
