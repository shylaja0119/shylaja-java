package com.sv.overridingtostring;

public class Laptop {
	double price;
	int noOfCoreProcessors;
	boolean isWindows;

	Laptop(double price, int noOfCoreProcessors, boolean isWindows) {
		super();
		this.price = price;
		this.noOfCoreProcessors = noOfCoreProcessors;
		this.isWindows = isWindows;
	}

	@Override
	public String toString() {
		return "Laptop" + " " + "-" + " " + price + "," + isWindows + "," + noOfCoreProcessors;
	}

	public static void main(String[] args) {
		Laptop laptop = new Laptop(45678, 4, false);
		System.out.println(laptop);
	}

}
