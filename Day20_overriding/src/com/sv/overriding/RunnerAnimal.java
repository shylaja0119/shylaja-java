package com.sv.overriding;

public class RunnerAnimal {
	public static void main(String[] args) {
		Dog d = new Dog();
		d.eat();

		Monkey m = new Monkey();
		m.eat();
	}
}
