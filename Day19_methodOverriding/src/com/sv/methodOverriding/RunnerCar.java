package com.sv.methodOverriding;

public class RunnerCar {
	public static void main(String[] args) {
		Car c = new Car();
		c.start();

		suv suv = new suv();
		suv.start();

		superCar superCar = new superCar();
		superCar.start();
	}
}
