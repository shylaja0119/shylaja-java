package com.sv.anstraction;

public class RunnerSim {
	public static void main(String[] args) {
		// we cannot create object of abstract class
		// but it can act as a reference to the sub class object
		AbstractSim s = new Sim();
		s.call();
		s.email();
		s.sms();
	}
}
