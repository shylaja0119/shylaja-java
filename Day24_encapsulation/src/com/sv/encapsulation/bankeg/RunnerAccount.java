package com.sv.encapsulation.bankeg;

public class RunnerAccount {
	public static void main(String[] args) {
		Account a = new Account();
		a.setAccNo(1122334455L);
		a.setAccHolderName("Shylaja V");
		a.setAccBalance(20500);
		a.setActive(true);

		System.out.println(a.getAccNo());

		System.out.println(a);
	}
}
