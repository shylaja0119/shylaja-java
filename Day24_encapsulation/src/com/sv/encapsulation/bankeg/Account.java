package com.sv.encapsulation.bankeg;

public class Account {
	private long accNo;
	private String accHolderName;
	private double accBalance;
	private boolean isActive;

	// generate Getters and setters from ALT + SHIFT + S + R

	public long getAccNo() {
		return accNo;
	}

	public void setAccNo(long accNo) {
		this.accNo = accNo;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public double getAccBalance() {
		return accBalance;
	}

	public void setAccBalance(double accBalance) {
		this.accBalance = accBalance;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", accHolderName=" + accHolderName + ", accBalance=" + accBalance
				+ ", isActive=" + isActive + "]";
	}

}
