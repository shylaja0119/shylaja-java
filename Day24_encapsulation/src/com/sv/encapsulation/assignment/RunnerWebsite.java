package com.sv.encapsulation.assignment;

public class RunnerWebsite {
	public static void main(String[] args) {

		AbstractWebsite aw = new Website();
		aw.login();
		aw.register();
		aw.logout();
	}

}
