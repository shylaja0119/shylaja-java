package com.sv.encapsulation.assignment;

public abstract class AbstractWebsite {
	abstract void login();

	void register() {
		System.out.println("registered");
	}

	void logout() {
		System.out.println("logout");
	}

}
