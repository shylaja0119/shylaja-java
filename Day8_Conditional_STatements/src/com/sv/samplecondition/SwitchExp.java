package com.sv.samplecondition;

public class SwitchExp {
	public static void main(String[] args) {
		char x = 'b';
		switch (x) {
		case 'b' : 
			System.out.println("beta");
//			break;
		default :
			System.out.println("default");
			break;
		case 'a' :
			System.out.println("alpha");
		}
		
		int y =3;
		switch (y) {
		case 1:
			System.out.println("one");
			break;
		case 2:
			System.out.println("two");
		case 3:
			System.out.println("three");
		default :
			System.out.println("default");
		}
		
	}

}
