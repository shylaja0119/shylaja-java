package com.sv.samplecondition;

public class ForLoopExamples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * // print negative integer values for (byte b = 10; b <= 10; b--) {
		 * System.out.println(b); }
		 */

		// 1-100
		for (int i = 1; i <= 100; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		// 100-1
		for (int i = 100; i >= 1; i--) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		// even number from 1-100
		for (int j = 2; j <= 100; j += 2) {
			System.out.print(j + " ");
		}
		System.out.println();
		
		// odd numbers from 100-1
		for (int j = 99; j >= 1; j -= 2) {
			System.out.print(j + " ");
		}
	}

}
