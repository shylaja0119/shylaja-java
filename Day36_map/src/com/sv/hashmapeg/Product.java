package com.sv.hashmapeg;

import java.util.HashMap;

public class Product {
	String productName;
	double price;
	String description;

	@Override
	public String toString() {
		return "Product [productName=" + productName + ", price=" + price + ", description=" + description + "]\n";
	}

	Product(String productName, double price, String description) {
		super();
		this.productName = productName;
		this.price = price;
		this.description = description;
	}

	public static void main(String[] args) {
		HashMap<Long, Product> proData = new HashMap<>();
		proData.put(5876898908L, new Product("abc", 789.0, "used for abc"));
		proData.put(5456768722L, new Product("lmn", 45780.0, "used for lmn"));

		System.out.println(proData);
	}

}
