package com.sv.hashmapeg;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Student {
	public static void main(String[] args) {
		HashMap<Integer, String> hm = new HashMap<>();
		hm.put(1, "alpha");
		hm.put(2, "beta");
		hm.put(3, "gamma");
		hm.put(4, "delta");

		System.out.println(hm);
		Set<Integer> Keys = hm.keySet();
		System.out.println(Keys); // printing keys

		// printing keys individually
		for (Integer i : Keys) {
			System.out.println(i);
		}

		System.out.println("printing values :");
		Collection<String> values = hm.values();
		for (String value : values) {
			System.out.println(value);
		}

	}
}
