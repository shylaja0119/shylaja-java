package com.sv.hashmapeg;

import java.util.HashMap;

public class HashMapExample {
	public static void main(String[] args) {
		HashMap<Integer, String> studentData = new HashMap<>();
		studentData.put(1, "Alpha");
		studentData.put(2, "Beta");
		studentData.put(1, "Charlie");
		studentData.put(null, "abc");
		studentData.put(null, "xyz");
		studentData.put(4, null);
		studentData.put(6, null);

		System.out.println(studentData);
		System.out.println("size : " + studentData.size());
		System.out.println(studentData.isEmpty());
		System.out.println(studentData.get(1));

	}
}
