package com.sv.day18Assignment;

public class Vehicle {
	String brand;
	String color;
	int price;

	Vehicle(String brand, String color, int price) {
		super();
		this.brand = brand;
		this.color = color;
		this.price = price;
	}

	/*
	 * Vehicle() { }
	 */

}
