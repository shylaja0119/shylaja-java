package com.sv.consChaining;

public class CarChain {

	String brand;
	String color;
	double price;

	CarChain(String brand) {
		// calling 3rd constructor
		this(brand, "Red", 0);
	}

	CarChain(String color, double price) {
		this(null, color, price);
	}

	CarChain(String brand, String color, double price) {
		this.brand = brand;
		this.color = color;
		this.price = price;
	}

	@Override
	public String toString() {
		return "CarChain [brand=" + brand + ", color=" + color + ", price=" + price + "]";
	}

	public static void main(String[] args) {
		CarChain c1 = new CarChain("Tata");
		System.out.println(c1);

		CarChain c2 = new CarChain("blue", 50000);
		System.out.println(c2);
	}

}
