package com.sv.arrayeg;

import java.util.ArrayList;

public class SampleArray {
	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<>(4);
		names.add("shylaja");
		names.add("Suditi");
		names.add("Kusum");

		System.out.println(names);

		for (String str : names) {
			System.out.println(str);
		}

	}
}
