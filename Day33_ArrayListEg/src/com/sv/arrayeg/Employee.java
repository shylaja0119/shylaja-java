package com.sv.arrayeg;

import java.util.ArrayList;
import java.util.Arrays;

public class Employee {
	int empNo;
	String name;
	int age;
	double salary;

	Employee(int empNo, String name, int age, double salary) {
		super();
		this.empNo = empNo;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", salary=" + salary + "]";
	}

	public static void main(String[] args) {
		Employee e1 = new Employee(100, "shylaja", 30, 10000);
		Employee e2 = new Employee(101, "Suditi", 27, 8000);
		Employee e3 = new Employee(103, "Kusum", 32, 5000);

		ArrayList<Employee> empList = new ArrayList<>(Arrays.asList(e1, e2, e3));

		for (Employee emp : empList) {
			System.out.println(emp);
		}

	}

}
