package com.sv.collectionassignment;

import java.util.ArrayList;

public class Restaurant {
	String name;
	int minPrice;
	boolean isAvailable;

	@Override
	public String toString() {
		return "Restaurant [name=" + name + ", minPrice=" + minPrice + ", isAvailable=" + isAvailable + "]\n";
	}

	Restaurant(String name, int minPrice, boolean isAvailable) {
		super();
		this.name = name;
		this.minPrice = minPrice;
		this.isAvailable = isAvailable;
	}

	public static void main(String[] args) {
		Restaurant r1 = new Restaurant("abc", 250, false);
		Restaurant r2 = new Restaurant("pqr", 500, true);
		Restaurant r3 = new Restaurant("xyz", 1000, true);

		ArrayList<Restaurant> ar = new ArrayList<>();
		ar.add(r1);
		ar.add(r3);
		ar.add(r2);
		System.out.println(ar);
	}

}
