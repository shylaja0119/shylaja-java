package com.sv.arraysort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArraySort {
	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<>(Arrays.asList(2, 5, 3, 4, 1));
		Collections.sort(al);
		System.out.println(al); // ascending order
		
		//descending order
		Collections.sort(al, Collections.reverseOrder());
		System.out.println(al);
		
	}
}
