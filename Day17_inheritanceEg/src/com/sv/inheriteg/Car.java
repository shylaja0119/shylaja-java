package com.sv.inheriteg;

public class Car extends Vehicle {
	boolean isAutomatic;

	void cruiseCtrl() {
		System.out.println("car in Cruise mode");

	}

	@Override
	public String toString() {
		return "Car [color=" + color + ", brand=" + brand + ", price=" + price + ", isAutomatic=" + isAutomatic + "]";
	}
}
