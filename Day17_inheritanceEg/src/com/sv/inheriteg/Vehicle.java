package com.sv.inheriteg;

public class Vehicle {
	String color;
	String brand;
	double price;

	void start() {
		System.out.println("vehicle started");
	}

	void stop() {
		System.out.println("vehicle stopped");
	}
}
