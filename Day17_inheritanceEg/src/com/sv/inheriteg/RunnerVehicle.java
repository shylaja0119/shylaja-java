package com.sv.inheriteg;

public class RunnerVehicle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car c = new Car();
		c.brand = "Honda";
		c.price = 5000;
		c.isAutomatic = true;
		System.out.println(c);
		c.start();
		c.cruiseCtrl();
		c.stop();

		System.out.println();

		Bike b = new Bike();
		b.brand = "Suzuki";
		b.color = "Black";
		System.out.println(b);
		b.start();

	}

}
