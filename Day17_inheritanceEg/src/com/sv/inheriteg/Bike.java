/**
 * 
 */
package com.sv.inheriteg;

/**
 * @author shylaja.venkatesha
 *
 */
public class Bike extends Vehicle {
	boolean isKickerPresent;

	@Override
	public String toString() {
		return "Bike [color=" + color + ", brand=" + brand + ", price=" + price + ", isKickerPresent=" + isKickerPresent
				+ "]";
	}

}
