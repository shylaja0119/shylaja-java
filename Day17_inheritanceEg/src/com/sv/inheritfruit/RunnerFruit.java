package com.sv.inheritfruit;

public class RunnerFruit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Apple a = new Apple();
		PineApple p = new PineApple();

		a.color = "Red";
		a.price = 100;
		a.noOfSeeds = 8;
		System.out.println(a);
		a.clean();
		a.prepareMilkshake();

		System.out.println();

		p.color = "Green";
		p.hasCrown = true;
		p.price = 50;
		System.out.println(p);
		p.clean();
		p.prepareSalad();
	}

}
