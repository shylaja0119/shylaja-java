package com.sv.inheritfruit;

public class Apple extends Fruit {
	int noOfSeeds;

	void prepareMilkshake() {
		System.out.println("Milkshake prepared");

	}

	@Override
	public String toString() {
		return "Apple [color=" + color + ", price=" + price + ", noOfSeeds=" + noOfSeeds + "]";
	}
}
