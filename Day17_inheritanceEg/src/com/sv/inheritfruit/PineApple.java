package com.sv.inheritfruit;

public class PineApple extends Fruit {
	boolean hasCrown;

	void prepareSalad() {
		System.out.println("Salad prepared");
	}

	@Override
	public String toString() {
		return "PineApple [color=" + color + ", price=" + price + ", hasCrown=" + hasCrown + "]";
	}
	
	

}
