package com.sv.chaining;

public class Car {
	String brand;
	String color;
	double price;

	Car(String brand) {
		super();
		this.brand = brand;
	}

	Car(String color, double price) {
		super();
		this.color = color;
		this.price = price;
	}

	Car(String brand, String color, double price) {
		super();
		this.brand = brand;
		this.color = color;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [brand=" + brand + ", color=" + color + ", price=" + price + "]";
	}

}
