package com.sv.day17Assignment;

public class Dog extends Animal {
	void bark() {
		System.out.println("dog is barking");
	}

	@Override
	public String toString() {
		return "Dog [breed=" + breed + ", gender=" + gender + ", color=" + color + ", name=" + name + ", age=" + age
				+ "]";
	}

}
