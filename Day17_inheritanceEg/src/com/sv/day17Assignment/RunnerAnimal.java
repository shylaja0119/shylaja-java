package com.sv.day17Assignment;

public class RunnerAnimal {

	public static void main(String[] args) {
		Cat c = new Cat();
		c.name = "kitten";
		c.age = 3;
		System.out.println(c);
		c.sleep();
		c.Scratch();

		Dog d = new Dog();
		d.name = "puppy";
		d.age = 4;
		System.out.println(d);
		d.eat();
		d.bark();
	}

}
