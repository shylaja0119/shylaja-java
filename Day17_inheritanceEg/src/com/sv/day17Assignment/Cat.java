package com.sv.day17Assignment;

public class Cat extends Animal {
	void Scratch() {
		System.out.println("cat is scratching");
	}

	@Override
	public String toString() {
		return "Cat [breed=" + breed + ", gender=" + gender + ", color=" + color + ", name=" + name + ", age=" + age
				+ "]";
	}

}
