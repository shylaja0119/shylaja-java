package com.sv.samplemethod;

public class day9Assignment {

	static double convertFahrenhietToCelcius(double fahrenhiet) {
		double celcius = (5.0 / 9.0) * (fahrenhiet - 32.0);
		return celcius;
	}

	static int checkLength(String input) {
		/* System.out.println(input.length());
		if (input.length() > 10) {
			return input.length();
		} else {
			return 0;
		} */
		return (input.length()) > 10 ? input.length() : 0;
	}

	public static void main(String[] args) {
		System.out.println(convertFahrenhietToCelcius(104.3));
		System.out.println(checkLength("Shylaja Venkatesha"));
		System.out.println(checkLength("SHylaja"));
	}
}
