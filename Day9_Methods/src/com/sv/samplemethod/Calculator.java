package com.sv.samplemethod;

public class Calculator {

	// method - reusability of code
	static int add(int num1, int num2) {
		int sum = num1 + num2;
		return sum;
	}

	static void printProduct(int m1, int m2) {
		System.out.println(m1 * m2);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Calculator.add(2, 3));
		System.out.println(add(6586587, 76987845));

		printProduct(20, 25);
		printProduct(35, 78);
	}

}
