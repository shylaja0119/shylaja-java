package com.sv.samplemethod;

public class maxAndMin {

	static int max(int a, int b) {
		return (a > b) ? a : b; // ternary operator
	}

	static int min(int x, int y) {
		return (x < y) ? x : y;
	}

	static void greet(String name) {
		System.out.println("Hello " + name);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int max = max(40, 98);
		System.out.println(max);

		int min = min(18789, 67655);
		System.out.println(min);

		greet("Shylaja");
	}

}
