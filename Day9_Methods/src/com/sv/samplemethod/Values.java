package com.sv.samplemethod;

public class Values {

	static void printValues(int num) {
		for (int i = 1; i <= num; i++)
			System.out.print(i + " ");
	}

	// simpleInterest method
	static double simpleInterest(int p, double t, double r) {
		return (p * t * r) / 100;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// print numbers
		printValues(30);
		System.out.println();
		printValues(20 - 10);
		System.out.println();

		double si = simpleInterest(10000, 2.5, 5.6);
		System.out.println(si);
	}

}
