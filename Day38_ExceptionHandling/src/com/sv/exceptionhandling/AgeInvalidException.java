package com.sv.exceptionhandling;

public class AgeInvalidException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static String message = "Age Invalid. it is less than 18 or more than 100";

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return message;
	}

}
