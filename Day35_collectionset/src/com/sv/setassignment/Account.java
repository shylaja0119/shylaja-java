package com.sv.setassignment;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Account {
	double accNo;
	String accName;
	double phNo;
	boolean isActive;
	int balance;

	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", accName=" + accName + ", phNo=" + phNo + ", isActive=" + isActive
				+ ", balance=" + balance + "]\n";
	}

	Account(double accNo, String accName) {
		super();
		this.accNo = accNo;
		this.accName = accName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accNo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		return Double.doubleToLongBits(accNo) == Double.doubleToLongBits(other.accNo);
	}

	public static void main(String[] args) {
		Account a1 = new Account(123, "shyl");
		Account a2 = new Account(345, "venk");
		Account a3 = new Account(345, "rekjk");
		Account a4 = new Account(786, "gfghfjg");
		Account a5 = new Account(786, "gfuy");
		Account a6 = new Account(786, "yuwqwxns");

		HashSet<Account> ha = new HashSet<>(Arrays.asList(a1, a2, a3, a4, a5, a6));

		System.out.println(ha);

	}

}
