package com.sv.seteg;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Student {
	String name;
	int id;

	Student(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", id=" + id + "]\n";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return id == other.id;
	}

	public static void main(String[] args) {
		Student s1 = new Student("Alpha", 1);
		Student s2 = new Student("Beta", 2);
		Student s3 = new Student("Gamma", 2);

		HashSet<Student> s = new HashSet<>(Arrays.asList(s1, s2, s3));
		System.out.println(s);
	}

}
