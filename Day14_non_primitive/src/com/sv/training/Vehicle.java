package com.sv.training;

public class Vehicle {
	String vehicleNo;
	String vehicleType;
	double costPerKm;
	boolean isAvailable;
	
	
	@Override
	public String toString() {
		return "Vehicle [vehicleNo=" + vehicleNo + ", vehicleType=" + vehicleType + ", costPerKm=" + costPerKm
				+ ", isAvailable=" + isAvailable + "]";
	}


	public static void main(String[] args) {
		Vehicle veh1 = new Vehicle();
		veh1.vehicleNo = "A100";
		veh1.costPerKm = 8.9;
		System.out.println(veh1);
		
		Vehicle veh2 = new Vehicle();
		veh2.vehicleType = "electric";
		veh2.isAvailable = true;
		System.out.println(veh2);
	}
}
