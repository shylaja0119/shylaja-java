package com.sv.training;

public class Facebook {

	// createUserProfile method
	User createUserProfile(String email, String pwd) {
		User user1 = new User(email, pwd);
		System.out.println("inside createUserProfile method");
		System.out.println(user1);
		return user1;
	}

	// updatePassword
	void updatePassword(User user1, String newPwd) {
		user1.pwd = newPwd;
	}

	public static void main(String[] args) {
		Facebook f = new Facebook();
		User user2 = f.createUserProfile("shyl@xyz.com", "shy12");
		System.out.println("inside main");
		System.out.println(user2);
		User user3 = f.createUserProfile("ven@xyz.com", "ven12");
		System.out.println("inside main");
		System.out.println(user3);

		f.updatePassword(user3, "abc@123");
		System.out.println("inside main");
		System.out.println(user3);

	}
}
