package com.sv.training;

public class User {
	String email;
	String pwd;

	public User(String email, String pwd) {

		this.email = email;
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", pwd=" + pwd + "]";
	}

}
